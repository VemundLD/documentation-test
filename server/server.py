from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import csv
import os
from datetime import datetime

class RequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        json_data = json.loads(post_data.decode('utf-8'))
        
        current_time = str(datetime.now())
        

        print("Received JSON data:")
        print(json_data)
        
        json_data['time'] = current_time

        self.save_to_csv(json_data)

        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Data received successfully!")

    def save_to_csv(self, data):
        csv_file_path = 'C:/python_venvs/MAS512_2023_AUTUMN/Assignment_1/server/data.csv'

        # Check if the CSV file already exists; if not, create it and write the header
        
        if not os.path.isfile(csv_file_path):
            with open(csv_file_path, 'w', newline='') as csv_file:
                csv_writer = csv.DictWriter(csv_file, fieldnames=data.keys())
                csv_writer.writeheader()

        # Append the received data to the CSV file
        with open(csv_file_path, 'a', newline='') as csv_file:
            csv_writer = csv.DictWriter(csv_file, fieldnames=data.keys())
            csv_writer.writerow(data)


if __name__ == '__main__':
    server_address = ('192.168.8.236', 8080)  # Use any available port
    httpd = HTTPServer(server_address, RequestHandler)
    print('Server running on port 8080...')
    httpd.serve_forever()