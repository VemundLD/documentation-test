#ifndef SENSOR_H
#define SENSOR_H
#include <DHTesp.h>
#include "Adafruit_CCS811.h"
//Global struct
struct sDHT
{
    float Temp;
    float Hum;
};
struct sQ
{
    float Temp;
    float TVOC;
    float eCO2;
};
struct sensor_data
{
    int soil1;
    int soil2;
    float hum;
    float temp;
    float quality_temp;
    float quality_TVOC;
    float quality_eCO2;
    float ph;
};

class sensor
{
    public:
    void sensor_init();
    std::pair<int, int> soil_sens();
    sDHT hum_temp_sens();
    sQ quality_sens();
    float PH_sens();
};

#endif // SENSOR_H