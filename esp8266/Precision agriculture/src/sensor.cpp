#include <sensor.h>
#include <Arduino.h>
#include <DHTesp.h>
#include <SPI.h>
#include <Wire.h>
#include "Adafruit_CCS811.h"
#include "DFRobot_PH.h"
#include <EEPROM.h>

//define pinns
//Soil sensor
#define pin_soil1_power 15
#define pin_soil2_power 3
#define pin_analog PIN_A0
//DHT11
#define pin_hum_temp_input 2
//SEN0161-V2
#define pin_PH_power 0
#define pin_analog PIN_A0            //pH meter Analog output to Arduino Analog Input 0

Adafruit_CCS811 ccs;
DHTesp dht;
DFRobot_PH ph;


void sensor::sensor_init()
{
    //soil sensors
    pinMode(pin_soil1_power,OUTPUT);
    digitalWrite(pin_soil1_power, LOW);
    pinMode(pin_soil2_power,OUTPUT);
    digitalWrite(pin_soil2_power, LOW);
    //DHT11
    dht.setup(pin_hum_temp_input, DHTesp::DHT11);
    //Air quality 3 click
    ccs.begin();
    //SEN0161-V2
    pinMode(pin_PH_power,OUTPUT);
    digitalWrite(pin_PH_power, LOW);
    ph.begin();
}
std::pair<int, int> sensor::soil_sens() 
{

	digitalWrite(pin_soil1_power, HIGH);	// Turn the sensor 1 ON
	delay(10);							// Allow power to settle
	int val1 = analogRead(pin_analog);	// Read the analog value form sensor
	digitalWrite(pin_soil1_power, LOW);		// Turn the sensor 1 OFF
    digitalWrite(pin_soil2_power, HIGH);	// Turn the sensor 2 ON
	delay(10);							// Allow power to settle
	int val2 = analogRead(pin_analog);	// Read the analog value form sensor
	digitalWrite(pin_soil2_power, LOW);		// Turn the sensor 2 OFF
    return std::make_pair(val1, val2); 		// Return analog moisture value
}
sDHT sensor::hum_temp_sens()
{
    sDHT hum_temp;
    hum_temp.Hum = dht.getHumidity();  // Read humidity from sensor
    hum_temp.Temp = dht.getTemperature(); // Read temperature from sensor
    return hum_temp;                   // Return Both 
}
sQ sensor::quality_sens()
{
    sQ quality;
    quality.Temp = ccs.calculateTemperature(); // Calculate temperature from sensor
    ccs.readData(); // Read sensor data
    quality.TVOC = ccs.getTVOC(); // Store TVOC data
    quality.eCO2 = ccs.geteCO2(); // Store CO2 data
    return quality;               // Return data 
}
float sensor::PH_sens()
{
    digitalWrite(pin_PH_power, HIGH);	// Turn the sensor ON
    delay(1000);	
    static unsigned long timepoint = millis();
    sensor sens;
    sQ temp = sens.quality_sens();
    float voltage;
    float temperature;
    float phValue;
    if(millis()-timepoint>1000U)
    {                  //time interval: 1s
        timepoint = millis();
        temperature = temp.Temp;         // read your temperature sensor to execute temperature compensation
        voltage = analogRead(pin_analog)/1024.0*3300;  // read the voltage
        //float phValue = ph.readPH(voltage,temperature);  // convert voltage to pH with temperature compensation
        
        float acidVoltage    = 2032.44;    //buffer solution 4.0 at 25C
        float neutralVoltage = 1500.0;     //buffer solution 7.0 at 25C
        float slope = (7.0-4.0)/((neutralVoltage-1500.0)/3.0 - (acidVoltage-1500.0)/3.0);  // two point: (_neutralVoltage,7.0),(_acidVoltage,4.0)
        float intercept =  7.0 - slope*(neutralVoltage-1500.0)/3.0;
        phValue = slope*(voltage-1500.0)/3.0+intercept;  //y = k*x + b
    }
    ph.calibration(voltage,temperature);           // calibration process by Serail CMD
    digitalWrite(pin_PH_power, LOW);	// Turn the sensor OFF
    return phValue;
}
