#include <sensor.h>
#include <string>
#include <IPAddress.h> 
class oled
{
    public:
    void oled_init();
    void oled_Display(const sensor_data& data);
    void oled_wifi_start(int count);
    void oled_wifi_connect(const IPAddress& ip);
};
