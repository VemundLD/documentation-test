#include <wifi.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <oled.h>


//statisk ip
const char* ssid     = "GL-MT300N-V2-fe9";
const char* password = "goodlife";
const char* host     = "http://192.168.8.130:8080/endpoint";

IPAddress ip(192,168,0,6);
IPAddress gatewayDNS(192,168,0,1); 
IPAddress netmask(255,255,255,0);
oled led;

void wifi::wifi_init()
{
    //WiFi.config(ip,gatewayDNS,netmask,gatewayDNS); //statisk ip
    WiFi.begin(ssid,password);
    int count = 0;
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(1000);
        led.oled_wifi_start(count);
        count ++;
    }

    IPAddress localIP = WiFi.localIP();
    led.oled_wifi_connect(localIP);
    /*
    Serial.println();
    Serial.println("Connected to WiFi");
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
    */
    
}
void wifi::wifi_transfer(const sensor_data& data)
{
    if (WiFi.status() == WL_CONNECTED) 
    {
        WiFiClient client;
        HTTPClient http;
        // Set the target server address
        
        //http.begin(client,"http://your-server.com/endpoint"); // Replace with your server URL
        //http.begin(client, "http://10.229.2.122/endpoint");
        http.begin(client, host);
        // Set the content type header for JSON data
        http.addHeader("Content-Type", "application/json");

        // Your JSON data to send
        
        String jsonData = "{\"Soil1\": "+String(data.soil1)+", \"Soil2\": "+String(data.soil2)+", \"Hum\": "+String(data.hum)+", \"Temp inn\": "+String(data.temp)+", \"Temp out\": "+String(data.quality_temp)+", \"eCO2\": "+String(data.quality_eCO2)+", \"TVOC\": "+String(data.quality_TVOC)+", \"PH\": "+String(data.ph)+"}";
        
        // Send a POST request with the JSON data
        int httpResponseCode = http.POST(jsonData);

        // Check the response code
        if (httpResponseCode > 0) 
        {
            String response = http.getString();
            Serial.println("HTTP Response: " + response);
        } 
        else 
        {
            Serial.println("HTTP Error: " + String(httpResponseCode));
        }

        http.end();
    }



}