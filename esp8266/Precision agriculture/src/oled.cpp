#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>
#include <oled.h>
#include <Arduino.h>


#define SCREEN_WIDTH 128 
#define SCREEN_HEIGHT 64
#define mosi_pin 13
#define sclk_pin 14
#define dc_pin 16
#define res_pin 12
#define cs_pin 1


Adafruit_SH1106G display = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT,mosi_pin, sclk_pin, dc_pin, res_pin, cs_pin);

void oled::oled_init ()
{
  display.begin(0, true);
  display.display();
  display.clearDisplay();
  
}

void oled::oled_Display(const sensor_data& data)
{
  display.clearDisplay();
  display.setTextSize(0.3);
  display.setTextColor(SH110X_WHITE);
  display.setCursor(0, 0);
  display.println(F("S_1: "));
  display.setCursor(30, 0);
  display.println(data.soil1);
  display.setCursor(0, 18);
  display.println(F("T_i: "));
  display.setCursor(30, 18);
  display.println(data.temp);
  display.setCursor(0, 36);
  display.println(F("Hum: "));
  display.setCursor(30, 36);
  display.println(data.hum);
  display.setCursor(0, 54);
  display.println(F("eCO2: "));
  display.setCursor(30, 54);
  display.println(data.quality_eCO2);
  
  display.setCursor(64, 0);
  display.println(F("S_2: "));
  display.setCursor(94, 0);
  display.println(data.soil2);
  display.setCursor(64, 18);
  display.println(F("T_o: "));
  display.setCursor(94, 18);
  display.println(data.quality_temp);
  display.setCursor(64, 36);
  display.println(F("TVOC: "));
  display.setCursor(94, 36);
  display.println(data.quality_TVOC);
  display.setCursor(64, 54);
  display.println(F("PH: "));
  display.setCursor(94, 54);
  display.println(data.ph);
  display.display();
}
void oled::oled_wifi_start(int count)
{
  display.setCursor(count*5, 0);
  display.setTextSize(0.5);
  display.setTextColor(SH110X_WHITE);
  display.println(F("."));
  display.display();
}
void oled::oled_wifi_connect(const IPAddress& ip)
{
  display.clearDisplay();
  display.setTextSize(0.5);
  display.setTextColor(SH110X_WHITE);
  display.setCursor(0, 0);
  display.println(F("Connected to WiFi"));
  display.println(F("IP Address: "));
  display.println(ip);
  display.display();
}
    