#include <Arduino.h>
#include <oled.h>
#include <sensor.h>
#include <wifi.h>


#define LED 2
// put function declarations here:



void setup() {
  //Set data rate
  Serial.begin(9600);
  wifi net;
  sensor sens;
  oled led;
  //initialize led display
  led.oled_init();
  //initialize wifi
  net.wifi_init();
  //initialize sensors
  sens.sensor_init();
}

void loop() {
  wifi net;
  oled led;
  sensor sens;
  sensor_data data;
  //Read data from DHT11 sensor
  sDHT hum_temp = sens.hum_temp_sens();
  //read data from Air quality 3 click
  sQ quality = sens.quality_sens();
  //read data from Soil sensor 1 and 2
  std::pair<int, int> soil_sens = sens.soil_sens();
  //Store sensor data in a struct
  data.soil1 = soil_sens.first;
  data.soil2 = soil_sens.second;
  data.hum = hum_temp.Hum;
  data.temp = hum_temp.Temp;
  data.quality_temp = quality.Temp;
  data.quality_eCO2 = quality.eCO2;
  data.quality_TVOC = quality.TVOC;
  //read data from pH senor
  data.ph = sens.PH_sens();
  //Print sensor data to led display
  led.oled_Display(data);
  //send data to server
  net.wifi_transfer(data);
  //Sampling delay
  delay(300000);
}

// put function definitions here:
