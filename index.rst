.. Presicion Agriculture documentation master file, created by
   sphinx-quickstart on Tue Oct 17 12:12:16 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MAS513 Precision Agriculture
=================================================



.. toctree::
   :maxdepth: 5
   :caption: Contents:

 Vidar Kolbeinsvik, Vemund Loe Davidsen

.. image:: src/images/assembly_2.jpg

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

Contents
--------

.. toctree::
   src/starting_seeds
   src/selecting_components
   src/custom_parts
   src/construction
   src/electronics
   src/Microcontroller
   src/code
   src/dashapp
   
Additional info
---------------
Check out the :doc:`starting_seeds` section for further information on seed germination This is an example of a cross reference.



.. note::
   This project is under active development. We're just learning.
