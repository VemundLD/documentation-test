Starting Seeds
==============

Seed Germination
----------------

Before starting to grow plants hydroponically, the seeds need to be germinated. For the seedling to be able to live in a hydroponic system,
its root needs to be developed and exposed enough for it to reach the nutrient solution.

Seed germination is simply done by keeping the seed moist in a dark, room temperature environment. A common technique is to place cotton in a
container, preferably air tight, and aplly water until the cotton is slightly damp. You need less moisture than you think, so it's a good idea to
use a spray bottle if you have one. 

When the cotton is moist, simply place your seeds on the cotton and close the container. Place the container somewhere dark, like in a cupboard, 
and check in with them periodically. The seeds should sprout within approximately ten days. 



Rockwool for hydroponics
------------------------
For hydroponics, one can use pieces of rockwool insulation to germinate seeds. This is done similarly to the cotton method. Soak the rockwool in
water until no more air comes out of it and place the seed on top. Prefabricated cubes with holes for the seeds can be purchased, but this is not
strictly necessary. A small piece of rockwool will do, as long as it is kept moist.

It can however be beneficial to use a larger piece, as the plants early roots will grow through the material and offer additional support to the
growing plant. When it comes time to place the seedling in net cups, the plant will also get support from the clay pebbles placed in the cup.

Another advantage of rockwool is that it wicks moisture. Because of this, seeds can be started directly in the hydroponic setup without needing
germinate separately first. It is not advised to use cotton for this, as it may delevop mold and rot. 


Once you've done this, it's time to :ref:`find a pipe <selecting_a_suitable_pipe>` to use