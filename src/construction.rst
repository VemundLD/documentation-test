Construction
============


Equipment needed
----------------

#. Hole saws Ø64, Ø32
#. Spiral drill bits Ø16, Ø12, Ø6.4 
#. Battery drill
#. Knife for deburring
#. Sandpaper
#. 8 M5*10 socket head cap screws
#. 8 M5 hex nuts


Cutting holes for adapters
--------------------------

There are three Ø64mm holes along the length of the pipe. The holes are spaced apart by 306mm starting 200mm from the filling end.
A hole saw Ø64mm is used to create the holes. The drilling accuracy is not critical. 

.. image:: images/Pipe_holes.png

After drilling, make sure to debur the edges with a knife, and sand them slightly with sand paper.


Drilling holes in the reservoir and end cap
-------------------------------------------
In the reservoir, six holes need to be drilled.
Four Ø6.2 holes to attach the feet. 
One Ø32 hole for the PH sensor adapter.
One Ø16 hole  for the pump hose adapter.

.. image:: images/lid.png

An Ø12 hole needs to be drilled in the end cap on the drain side, 24mm from the center.

.. image:: images/end_cap_dwg.png

An Ø12 hole needs to be drilled in the reservoir container for the return hose.

.. image:: images/box.png



Assembly
--------
When all holes are drilled, attach the feet to the reservoir lid with four M5 screws and nuts. Place all the printed adapters in the appropriate holes. The pipe is placed on the feet and screwed in place with the remaining 5 screws and nuts. Finally, measure out appropriate lengths of plastic tubing and attach them to the filling and draining caps, as well as to the reservoir. 





