Selecting components
====================
.. image:: images/3d_model.png

The planting tray is the main hardware of the hydroponic system. It consists of a PVC pipe with an end cap, 3D printed adapters and net cups.
The net cups are special hydroponic pots that allow root to grow through them in order for the roots to access the nutrient solution.




Pipe
----
.. image:: images/Pipe_drawing.png

The main part of the planting tray is the pipe. The pipe has an effective length of 1000mm, inned siameter of Ø70mm and outer diameter Ø75mm. Although dimensions are not critical, it's best to find a pipe as close as possible to the one used here.
Additionally, the pipe must have a sleeve at one end with a gasket and end cap. This is to make sure the drain side does not leak as water runs through the pipe.




Cap for filling side
--------------------
A cap is also needed for the filling side of the pipe, mainly to attach the filling hose, but also to prevent spillage if the water level should rise temporarily. The end cap should fit snuggly around the filling end of the pipe, but it can be sealed with silicone if necessary. The filling side cap needs to have a hode drilled into it to accommodate the hose. The size of this hole depends on the diameter of the fittings used for the hose. 





Net cups
--------
.. image:: images/net_cup.png

Net cups can be purchaed at some specialist shops, but are easiest to find on the internet. In Norway, `Gartnerbutikken <https://www.gartnerbutikken.no/products/nettpotte-for-aquaplate?gclid=CjwKCAiAx_GqBhBQEiwAlDNAZrGSRnnQdODtXailQiJObxopuAyidsiq2tvp83SuF5GcgW85UO3j6BoCnzMQAvD_BwE>`_. sells net cups in various sizes.




Clear tubing for filling and draining
-------------------------------------
Lengths of clear plastic tubing of outer diameter Ø8mm is used to connect the water reservoir to the top of the planter and from the bottom of the planter back to the water tank. The dimensions are not critical. The most important thing is that it fits the pump. 

.. image:: images/plastslange.jpg



Fittings to attach plastic tubes to pipe
----------------------------------------
Fittings are needed to attach the plastic tubing to the end caps on the pipe. These should be available at the same hardware store where the other components are found. 
