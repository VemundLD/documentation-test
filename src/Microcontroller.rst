Microcontroller
============


Development board
----------
Wemos D1 development board is used for meassuring and transfering sensordata. It is equiped with a esp8266 microcontroller and wifi. The controller have 1 analog and 11 digital pins that supports I2C and SPI.


.. image:: images/controller.jpeg

Library
----------
#. DHT sensor library for ESPx@^1.19
#. CCS811 Library@^1.1.1 
#. DFRobot_PH@^1.0.0
#. Adafruit SH110X@^2.1.10


Pinout
----------
==================   ============    ============
Name                 GPIO            Physical pin
==================   ============    ============
Analog sensor        A0              A0
Soil 2 power         3               D00             
Oled CS              1               D01
Oled DC              16              D02
PH power             0               D08
DHT11 input          2               D09
Soil 1 power         15              D10
Oled MOSI            13              D11
Oled RES             12              D12
Oled SCLK            14              D13
Air quality SDA      4               D14
Air quality SCK      5               D15        
==================   ============    ============




.. image:: images/Pinout.jpeg





