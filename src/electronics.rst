Electronics
===========
A variety of sensors are used to measure the system. These are run by an ESP8266 microcontroller. In addition to the sensors, a small pump
is used to pump water into the planting tray. 

Equipment
----------
#. Wemos D1 esp8266
#. Air quality 3 click
#. DHT11
#. Soil Moisture sensor x2
#. SH1106
#. SEN0161-V2
#. Motor pump 5V
#. 1N5817 x3

DHT11 Temperature and humidity sensor
---------------------------------------
the DHT11 uses a DFR0067 sensor to messure humidity and temperature. It is placed inside the planting tray to monitor the ambient condition in the pipe.
In hydroponics it is enough to have a humid atmosphere to keep the roots alive. The clay pebbles inside wick and hold moisture so it can
evaporate and keep a stable environment.
The DHT11 also measures temperature. The temperature inside is monitored in order to prevent the plant's root systems getting too hot.
The DHT11 is connected with 3.3v power, ground and digital output to D9 pin that comunicate with serial data.

Soil moisture sensor
--------------------------
A piece of rockwool is placed inside the pipe along with clay pebbles. The moisture sensor is placed in this cube to monitor how much water is
inside. When the rockwool cube dries out, the reservoir is too dry and needs to be filled. 2 sensors are used, one fore each plant. The sensor is connected with 3.3v power on D0 and D10, ground and analog output connected to a0 pin. 

Air quality 3 click
----------
Air quality 3 click measures the ambient air quality. It senses temperature, CO2 levels and TVOC. TVOC stands for total volatile oraganic compund and is organic chemicals that have turned into gasses.
It is placed outside close to the planting tray. The sesor is connected with 3.3v power, ground. THes sensor uses SPI where SDA and SCK  is connected to pin D12-D13. It has a wakeup pin that is connected to 3.3v to run.

SEN0161-V2
------------------
SEN0161-V2 is the pH sensor placed in the water reservoir witch measures the acidity or basicity of the solution. It is importen for the plants to have the correct pH values for them to grow. The sensor uses a adpter for coaxial cable. It is connected with 3.3v on D8, ground and analog out to a0 pin.

Motor pump 5V
---------
Basic pump with and input voltage of 3-5v for the circulation of water from the reservoir trough the planting tray and back to the reservoir. The pump is connected to a static power supply for continuous circulation.

Analog input
----------
In this project we have three sensor with analog output but the Wemos D1 only have one analog input a0.
To circumvent this limitation the sensor shares a0. 
To prevent the sensors to interfere with eachother the sensors power is connected to digital pins where the micro controller go in order trough the sensors where it turns on sensor, read analog data, turn of sensor and repeat for next sensor.
A diode is used for these sensor to block voltage from flowing back to the ouputs. 1N5817 diods is used for there low forward voltage.

SH1106
----------
An SH1106 oled display is used for on site monitorization. It has a resolution of 128x64 with a I2C interface.

Electrical diagram
----------
.. image:: images/circuit.png
