Custom parts
============



Filling end cap
---------------
The end cap tp seal off the filling end of the pipe. 

.. image:: images/filling_cap_model.png

Feet
----
The feet have holes and slots for M5 hexagonal nuts and hexagonal socked head screws.

.. image:: images/foot.png


Fittings
--------
Some fittings have to be made in order to attach the plastic tubing and the PH sensor.

.. image:: images/fittings.png
