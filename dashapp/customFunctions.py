from dash import Dash, html, dash_table, dcc, callback, Output, Input
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import datetime as dt

def createmainSubplot(df):
    mainSubplotFigure = make_subplots(rows=4, cols=2, subplot_titles=['Soil 1', 'Soil 2', 'Temperature Inside', 'Temperature Outside', 'Humidity', 'eCO2', 'TVOC', 'PH'])
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['Soil 1']),              row=1, col=1)
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['Soil 2']),              row=1, col=2)
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['Temperature inside']),  row=2, col=1)
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['Temperature outside']), row=2, col=2)
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['Humidity']),            row=3, col=1)
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['eCO2']),                row=3, col=2)
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['TVOC']),                row=4, col=1)
    mainSubplotFigure.add_trace(go.Scatter(x=df['time'], y=df['PH']),                  row=4, col=2)

    mainSubplotFigure.update_yaxes(title_text="",    row=1, col=1) #Soil 1 plot
    mainSubplotFigure.update_yaxes(title_text="",    row=1, col=2) #Soil 2 plot
    mainSubplotFigure.update_yaxes(title_text="°C",  row=2, col=1) #Temp inside plot
    mainSubplotFigure.update_yaxes(title_text="°C",  row=2, col=2) #Temp outside
    mainSubplotFigure.update_yaxes(title_text="%",   row=3, col=1) #Humidity
    mainSubplotFigure.update_yaxes(title_text="PPM", row=3, col=2) #CO2
    mainSubplotFigure.update_yaxes(title_text="PPB", row=4, col=1) #TVOC
    mainSubplotFigure.update_yaxes(title_text="",    row=4, col=2) #PH

    mainSubplotFigure.update_layout(showlegend=False)
    mainSubplotFigure.update_layout(height=600)

    return mainSubplotFigure


def createSubplotGraphHeadlineTimestamps():
    delta = dt.timedelta(minutes=5)
    currentTime = dt.datetime.now()
    return f"Time {currentTime - delta} -- {currentTime}"


if __name__ == '__main__':
    pass