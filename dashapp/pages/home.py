import dash
from dash import Dash, html, dash_table, dcc, callback, Output, Input
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import datetime as dt

dash.register_page(__name__, name="Home", path='/')


layout = html.Div(
    children=[
        html.H1(
            children='Wecome to the MASXXX data view app',
            style={'textAlign':'center'}
        ),

        html.Div(
            children="In the top left corner you find buttons that take you to individual pages to view specific data.",
            style={'textAlign':'center'}
        ),

        html.Img(src='assets/3.jpg', style={'width':'50%'})
    ]
)

