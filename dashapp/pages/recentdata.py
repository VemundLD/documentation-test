import dash
from dash import Dash, html, dash_table, dcc, callback, Output, Input
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import datetime as dt
from customFunctions import *


#df = pd.read_csv('timedata.txt')
df = pd.read_csv('server/data.csv')
df = df[700:-1]

dash.register_page(__name__, name="Recent Data", order=0)


layout = html.Div(
    children=[
        html.H1(
            id='header_text',
            children='Recent Data'),

        html.Div(
            children=[
                dcc.Graph(
                    id='recent_data_subplots',
                    figure=createmainSubplot(df)
                ),

                dcc.Interval(
                    id='update_interval',
                    interval=300_000,
                    n_intervals=0
                )
            ]
        )
    ]
)



@callback(
    Output(component_id='recent_data_subplots', component_property='figure'),
    Input(component_id='update_interval', component_property='n_intervals')
)
def updateRecentData(n_intervals):
    #Reload dataframe
    df = pd.read_csv('server/data.csv')

    dfRows = df.shape[0]
    if dfRows > 20:
        df = df[dfRows - 20 : dfRows]

    return createmainSubplot(df)

    

