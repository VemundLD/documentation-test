import dash
from dash import Dash, html, dash_table, dcc, callback, Output, Input
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import datetime as dt



dash.register_page(__name__, name="Pictures", order=3)

layout = html.Div(
    children=[
        html.H1('Pictures'),
        html.H3('I propose a slider to select a date, and view the picture taken that day'),

        dcc.Slider(
            id='date_slider',
            min=0,
            max=110,
            value=0,
            step=1,
            updatemode='drag'
        ),

        html.Div(
            children=[
                html.Img(
                    id='image',
                    src='assets/plant_pictures/0.png'
                ),
            ],
            style={
                'display':'inline-block',
                'width':'100%'
            }
                
        ),

        
    ]
)

@callback(
    Output(component_id='image', component_property='src'),
    Input(component_id='date_slider', component_property='value')
)
def updateImage(imgNumber):
    return f"assets/plant_pictures/{imgNumber}.png"