import dash
from dash import Dash, html, dash_table, dcc, callback, Output, Input
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import datetime as dt
from customFunctions import *

df = pd.read_csv('server/data.csv')

#Soil 1,Soil 2,Humidity,Temperature inside,Temperature outside,eCO2,TVOC,PH,time
yAxisLabels = {
    "Soil 1" : "",
    "Soil 2" : "",
    "Humidity" : "%RH",
    "Temperature inside" : "°C",
    "Temperature outside" : "°C",
    "eCO2" : "PPM",
    "TVOC" : "PPB",
    "PH" : "pH"
}

titles = {
    "Soil 1" : "Moisture in rockwool, plant 1",
    "Soil 2" : "Moisture in rockwool, plant 2",
    "Humidity" : "Relative humidity inside planting tray",
    "Temperature inside" : "Temperature inside planting tray",
    "Temperature outside" : "Ambient temperature",
    "eCO2" : "CO2 in ambient air",
    "TVOC" : "Volatile organic compounds in ambient air",
    "PH" : "pH in water reservoir"}

dash.register_page(__name__, name="Historical Data", order=1)

layout = html.Div(
    children=[
        html.H1('Historical Data'),

        html.Div(
            "This is the page for viewing selected historical data"
        ),

        html.Div(
            children=[
                dcc.Dropdown(
                    id='selection_dropdown',
                    options=df.columns[:-1],
                    value="PH"
                )
            ]
        ),

        html.Div(
            id='graph_container',
            children=[
                dcc.Graph(
                    id="historical_graph",
                    figure=px.line(x = df['time'], y = df['Humidity'])
                )
            ]
        ),
    ]
)


@callback(
    Output(component_id='historical_graph', component_property='figure'),
    Input(component_id='selection_dropdown', component_property='value')
)
def updateGraph(selectedColumn):
    if selectedColumn is None:
        fig =  px.line(x = df['time'], y = df['PH'])
        #fig.update_layout(title="HÆSTKUK", font=dict(size=20))
    else:    
        fig =  px.line(x = df['time'], y = df[selectedColumn])
        #fig.update_layout(title="HÆSTKUK", font=dict(size=20))

    fontsize = 30
    fig.update_layout(yaxis=dict(tickfont=dict(size=fontsize)), xaxis=dict(tickfont=dict(size=fontsize)))
    fig.update_layout(title=titles[selectedColumn], font=dict(size=20))
    fig.update_xaxes(title="Time")
    fig.update_yaxes(title=yAxisLabels[selectedColumn])
    fig.update_xaxes(title_font=dict(size=fontsize))
    fig.update_yaxes(title_font=dict(size=fontsize))
    
    return fig
