import dash
from dash import Dash, html, dash_table, dcc, callback, Output, Input
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
from customFunctions import *



app = Dash(__name__, use_pages=True)

app.layout = html.Div(
    children=[
        html.H1(
            children=[
                "MAS513 DataView"
            ],
            style={
                'textAlign':'center'
            }
        ),

        #Create links to other pages
        html.Div(
            children=[
                html.Div(
                    children=[
                        dcc.Link(html.Button(page['name'], style={'width':'150px'}), href=page["relative_path"], style={'padding':'5px'}) for page in dash.page_registry.values() if page['name'] != 'Home'
                    ],
                )
            ]          
        ),

        #Show the contents of the current page
        dash.page_container,
    
    ] 
)



if __name__ == '__main__':
    app.run(debug=True)